(load "raylib.lsp")

(define (main)
  (InitWindow 800 450 "model animation")
  (setq camera (Camera3D* '((10 10 10) (0 0 0) (0 1 0) 45 0))
	model (LoadModel "resources/guy.iqm")
	model& (pack Model (pack Matrix (model 0)) (1 model))
	texture (LoadTexture "resources/guytex.png")
	position (pack Vector3 0 0 0)
	anims-count (pack "lu" 0)
	anims (LoadModelAnimations "resources/guyanim.iqm" anims-count)
	anim-frame-count ((unpack ModelAnimation anims) 1)
	anim-frame-counter 0)
  (SetMaterialTexture (model 4) 0 (pack Texture texture))
  (SetCameraMode camera 1)
  (SetTargetFPS 60)

  (while (zero? (WindowShouldClose))
	 (UpdateCamera camera)
	 (when (odd? (IsKeyDown 32))
	   (++ anim-frame-counter)
	   (UpdateModelAnimation model& anims anim-frame-counter)
	   (and (>= anim-frame-counter anim-frame-count)
		(setq anim-frame-counter 0)))
	 (BeginDrawing)
	   (ClearBackground RAYWHITE&)
	   (BeginMode3D camera)
	     (DrawModelEx model& position (pack Vector3 1 0 0)
			  -90 (pack Vector3 1 1 1) WHITE&)
	     (DrawGrid 10 1)
	   (EndMode3D)
	   (DrawText "PRESS SPACE TO PLAY MODEL ANIMATION" 10 10 20 MAROON&)
	   (DrawText "(c) Guy IQM 3D model by @culacant" 600 430 10 GRAY&)
	 (EndDrawing))
  (CloseWindow))
