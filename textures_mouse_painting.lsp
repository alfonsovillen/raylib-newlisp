(load "raylib.lsp")

(define (main)
  (letn (

    ; colors to choose from and their placements

    (color-recs
      (map
        (fn (c)
          (list c (pack Rectangle (add 10 (mul 30 $idx) (mul 2 $idx)) 10 30 30)))
        (list RAYWHITE& YELLOW& GOLD& ORANGE& PINK& RED& MAROON& GREEN& LIME&
              DARKGREEN& SKYBLUE& BLUE& DARKBLUE& PURPLE& VIOLET& DARKPURPLE&
              BEIGE& BROWN& DARKBROWN& LIGHTGRAY& GRAY& DARKGRAY& BLACK&)))
    (max-colors (length color-recs))
    (color-selected 0)
    (color-prev color-selected)
    (color-hover nil)
    (brush-size (float 20))
    (mouse-pressed nil)
    (screen-width 800)
    (screen-height 450)
    (save-rec (pack Rectangle 750 10 40 30))
    (save-hover nil)
    (save-message nil)
    (save-counter 0)
    (target nil)
    (target& nil)
    (target.texture& nil)
    (target-rec nil)
    (mouse-pos nil)
    (mouse-pos& nil)
    (vec00 (pack Vector2 0 0)))

  ; create a window

  (InitWindow screen-width screen-height "mouse painting")

  ; create a RenderTexture to use as a canvas

  (setq
    target (LoadRenderTexture screen-width screen-height)
    target& (RenderTexture* target)
    target.texture& (pack Texture (RenderTexture.texture target))
    target-rec (pack Rectangle 0 0 (Texture.width (RenderTexture.texture target))
		     (Texture.height (RenderTexture.texture target))))

  ; clear render texture before entering the game loop

  (BeginTextureMode target&)
  (ClearBackground (color-recs 0 0))
  (EndTextureMode)

  ; set our game to run at 120 frames per second

  (SetTargetFPS 120)

  ; main game loop

  (while (zero? (WindowShouldClose)) ; detect window close button or ESC key

	 ; update

	 (setq mouse-pos (GetMousePosition)
	       mouse-pos& (pack Vector2 mouse-pos)
	       color-hover nil)

	 ; move between colors with keys

	 (if
	   (odd? (IsKeyPressed KEY:RIGHT)) (min (++ color-selected) max-colors)
	   (odd? (IsKeyPressed KEY:LEFT)) (max (-- color-selected) 0))

	 ; choose color with mouse

	 (dolist (c color-recs
		    (and (odd? (CheckCollisionPointRec mouse-pos& (c 1)))
			 (setq color-hover $idx))))
	 (and color-hover
	      (odd? (IsMouseButtonPressed MOUSE_BUTTON:LEFT))
	      (setq color-selected color-hover
		    color-prev color-selected))

	 ; change brush size

	 (setq brush-size
	       (min (max 2 (add brush-size (mul (GetMouseWheelMove) 5))) 50))
	 (when (odd? (IsKeyPressed KEY:C))
	   (BeginTextureMode target&)
	   (ClearBackground (color-recs 0 0))
	   (EndTextureMode))

	 ; paint circle into render texture
	 ; NOTE: the Y coordinate is upside down

	 (when (or (odd? (IsMouseButtonDown MOUSE_BUTTON:LEFT))
		 (= GESTURE:DRAG (GetGestureDetected)))
	   (when (> (mouse-pos 1) 50)
	     (BeginTextureMode target&)
	     (DrawCircle (mouse-pos 0) (- screen-height (mouse-pos 1))
			 brush-size (color-recs color-selected 0))
	     (EndTextureMode)))

	 ; erase circle from render texture
	 ; NOTE: the Y coordinate is upside down

	 (cond
	   ((odd? (IsMouseButtonDown MOUSE_BUTTON:RIGHT))
	    (unless mouse-pressed
	      (setq color-prev color-selected
	            color-selected 0))
	    (setq mouse-pressed true)
	    (when (> (mouse-pos 1) 50)
	      (BeginTextureMode target&)
	      (DrawCircle (mouse-pos 0) (sub screen-height (mouse-pos 1))
			  brush-size (color-recs 0 0))
	      (EndTextureMode)))
	   ((and mouse-pressed (odd? (IsMouseButtonReleased MOUSE_BUTTON:RIGHT)))
	    (setq color-selected color-prev
		  mouse-pressed nil)))

	 ; check mouse hover save button

	 (setq save-hover (odd? (CheckCollisionPointRec mouse-pos& save-rec)))

	 ; saving logic

	 (when (or (and save-hover (odd? (IsMouseButtonReleased MOUSE_BUTTON:LEFT)))
		   (odd? (IsKeyPressed KEY:S)))
	   (let (image (pack Image (LoadImageFromTexture target.texture&)))
	     (ExportImage image "my_amazing_texture_painting.png")
	     (UnloadImage image)
	     (setq save-message true)))

	 ; on saving, show a full screen message for 2 seconds

	 (when save-message
	   (++ save-counter)
	   (when (> save-counter 240)
	     (setq save-message nil
		   save-counter 0)))

	 ; draw

	 (BeginDrawing)
	   (ClearBackground RAYWHITE&)
	   (DrawTextureRec target.texture& target-rec vec00 WHITE&)

	   ; draw a circle for reference

	   (when (> (mouse-pos 1) 50)
	     (if (odd? (IsMouseButtonDown MOUSE_BUTTON:RIGHT))
	       (DrawCircleLines (mouse-pos 0) (mouse-pos 1) brush-size GRAY&)
	       (DrawCircle (GetMouseX) (GetMouseY) brush-size
			   (color-recs color-selected 0))))

	   ; draw top panel

	   (DrawRectangle 0 0 (GetScreenWidth) 50 RAYWHITE&)
	   (DrawLine 0 50 (GetScreenWidth) 50 LIGHTGRAY&)

	   ; draw color selection rectangles

	   (dolist (c color-recs)
	     (DrawRectangleRec (c 1) (c 0)))
	   (DrawRectangleLines 10 10 30 30 LIGHTGRAY&)
	   (when color-hover
	        (DrawRectangleRec (color-recs color-hover 1)
				  (pack Color (Fade WHITE& 0,6))))
	   (DrawRectangleLinesEx (color-recs color-selected 1) 2 BLACK&)

	   ; draw save image button

	   (DrawRectangleLinesEx save-rec 2 (if save-hover RED& BLACK&))
	   (DrawText "SAVE!" 755 20 10 (if save-hover RED& BLACK&))

	   ; draw save image message

	   (when save-message
	     (DrawRectangle 0 0 (GetScreenWidth) (GetScreenHeight)
			    (pack Color (Fade RAYWHITE& 0,8)))
	     (DrawRectangle 0 150 (GetScreenWidth) 80 BLACK&)
	     (DrawText "IMAGE SAVED:  my_amazing_texture_painting.png"
		       150 180 20 RAYWHITE&))
       (EndDrawing))

  ; cleaning-up

  (UnloadRenderTexture target&)
  (CloseWindow)))

