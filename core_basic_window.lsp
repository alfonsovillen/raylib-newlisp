(load "raylib.lsp")

(InitWindow 800 450 "raylib example")
(SetTargetFPS 60)
(while (zero? (WindowShouldClose))
       (BeginDrawing)
       (DrawText "Congrats! You created your first window!"
		 190 200 20 LIGHTGRAY&)
       (EndDrawing))
(CloseWindow)
(exit)

