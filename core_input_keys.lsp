(load "raylib.lsp")

(InitWindow 800 450 "raylib example")
(SetTargetFPS 60)
(setq ball-x 400
      ball-y 225)
(while (zero? (WindowShouldClose))
       (if
	 (odd? (IsKeyDown KEY:LEFT))
	   (-- ball-x 2)
	 (odd? (IsKeyDown KEY:RIGHT))
	   (++  ball-x 2)
	 (odd? (IsKeyDown KEY:UP))
	   (-- ball-y 2)
	 (odd? (IsKeyDown KEY:DOWN))
	   (++ ball-y 2))
       (BeginDrawing)
       (ClearBackground RAYWHITE&)
       (DrawText "move the ball with arrow keys"
		 10 10 20 DARKGRAY&)
       (DrawCircle ball-x ball-y 50 MAROON&)
       (EndDrawing))
(CloseWindow)
(exit)

