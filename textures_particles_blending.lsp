(load "raylib.lsp")

(define (new-particle , x y)
  (map set '(x y) (GetMousePosition))
  (list
    x y ; position 
    (pack Color (rand 256) (rand 256) (rand 256) 255) ; color
    (float 1) ; alpha
    (div (inc (rand 30)) 20) ; size
    (random 1 360))) ; rotation

(define (initialize)
  (define max-particles 200)
  (define screen-width 800)
  (define screen-height 450)
  (define modes (list BLEND:ALPHA BLEND:ADDITIVE))
  (InitWindow screen-width screen-height "raylib [textures] example - particles blending")
  (define mouse-tail '())
  (define gravity (div 3 2))
  (define smoke (LoadTexture "resources/spark_flame.png"))
  (define smoke-width (float (Texture.width smoke)))
  (define smoke-height (float (Texture.height smoke)))
  (define smoke-shape& (pack Rectangle 0 0 smoke-width smoke-height))
  (define smoke& (pack Texture smoke))
  (define blending BLEND:ALPHA)
  (SetTargetFPS 60))

(define (loop , tail2 (end 0))
  (while (zero? (WindowShouldClose))
	 (setq tail2 '())
	 (dolist (p mouse-tail)
	   (if (> (p 3))
	     (push
	       (list
		 (p 0)
		 (inc (p 1) gravity)
		 (p 2)
		 (dec (p 3) 0,005)
		 (p 4)
		 (inc (p 5) 2))
	       tail2 -1)
	     (-- end)))
	 (swap mouse-tail tail2)
	 (and (< end max-particles)
	   (push (new-particle) mouse-tail -1)
	   (++ end))
	 (or (zero? (IsKeyPressed KEY:SPACE))
	     (setq blending (first (rotate modes))))
	 (BeginDrawing)
	   (ClearBackground DARKGRAY&)
	   (BeginBlendMode blending)
	     (dolist (p mouse-tail)
	       (DrawTexturePro
		 smoke&
	  	 smoke-shape&
		 (pack Rectangle (p 0) (p 1) (mul smoke-width (p 4)) (mul smoke-height (p 4)))
		 (pack Vector2 (div (mul smoke-width (p 4)) 2) (div (mul smoke-height (p 4)) 2))
		 (p 5)
		 (pack Color (Fade (p 2) (p 3)))))
	     (EndBlendMode)
	   (DrawText "PRESS SPACE to CHANGE BLENDING MODE" 180 20 20 BLACK&)
	   (if (= blending BLEND:ALPHA)
	     (DrawText "ALPHA BLENDING" 290 (- screen-height 40) 20 BLACK&)
	     (DrawText "ADDITIVE BLENDING" 280 (- screen-height 40) 20 RAYWHITE&))
	   (EndDrawing)))

(define (cleanup)
  (UnloadTexture smoke&)
  (CloseWindow))

(define (main)
  (initialize)
  (or (catch (loop) 'error)
      (println error))
  (cleanup))

